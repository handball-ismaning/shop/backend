module.exports = {
  load: {
    before: [
      "boom",
      "sentry",
      "responseTime",
      "logger",
      "cors",
      "responses",
      "gzip"
    ],
  },
  settings: {
    sentry: {
      enabled: true,
    },
    cors: {
      enabled: false, // tried true and false
      origin: "*",
    },
  }
}
