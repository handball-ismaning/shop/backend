'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async getAndIncrementNextOrderNumber() {
    let configData = await strapi.query('config').find()
    if (!configData || configData.length === 0) {
      throw new Error("No config data found")
    }
    configData = configData[0]
    const number = configData.nextOrderNumber
    await strapi.query('config').update({ _id: configData._id }, {
      ...configData,
      nextOrderNumber: number + 1
    })
    return number
  },
  async getOrderNotificationEmail() {
    let configData = await strapi.query('config').find()
    if (!configData || configData.length === 0) {
      throw new Error("No config data found")
    }
    configData = configData[0]
    return configData.notificationEmail
  }
};
