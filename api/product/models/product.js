'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate (result) {
      await strapi.api.product.services.product.updateStockCount(result._id)
    },
    async afterUpdate (result) {
      await strapi.api.product.services.product.updateStockCount(result._id)
    }
  }
};
