'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async updateStockCount(id) {
    const productModel = await strapi.query('product').model
    const product = await productModel.findOne({ _id: id }).populate({ path: 'variants' })
    const inStock = product.variants.reduce((acc, variant) => {
      return acc + variant.inStock
    }, 0)
    if (inStock !== product.inStock) {
      product.inStock = inStock
      await product.save()
    }
  }
};
