'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async create(ctx) {
    let entity;
    let data;
    let files;
    if (ctx.is('multipart')) {
      const multipartData = parseMultipartData(ctx);
      data = multipartData.data;
      files = multipartData.files;
    } else {
      data = ctx.request.body;
    }
    const variantIds = data.Item.map(item => {
      return item.product
    })
    const productVariants = await strapi.query('variant').find({ _id_in: variantIds })
    const variantLookup = productVariants.reduce((acc, variant) => {
      return {
        ...acc,
        [variant._id]: variant
      }
    }, {})
    let enoughItemsInStock = true
    data.Item.forEach((item) => {
      const variant = variantLookup[item.product]
      if (variant.inStock < item.amount) {
        enoughItemsInStock = false
      }
    })
    if (!enoughItemsInStock) {
      return ctx.badRequest('Die gewählten Produkte sind nicht in ausreichender Stückzahl verfügbar')
    }
    for (const item of data.Item) {
      const variant = variantLookup[item.product]
      await strapi.query('variant').update({ id: item.product }, {
        ...variant,
        inStock: variant.inStock - item.amount
      })
    }
    entity = await strapi.services.order.create(data, { files });
    return sanitizeEntity(entity, { model: strapi.models.order });
  },
};
