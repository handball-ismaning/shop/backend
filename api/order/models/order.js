'use strict';
const Sentry = require('@sentry/node');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    beforeCreate: async data => {
      data.userName = data.user.firstName + " " + data.user.lastName
      data.number = await strapi.services.config.getAndIncrementNextOrderNumber()
    },
    beforeUpdate: async (params, data) => {
      data.userName = data.user.firstName + " " + data.user.lastName
    },
    afterCreate: async (result) => {
      Sentry.configureScope(function(scope) {
        scope.setTag("ordernumber", result.number);
        scope.setUser({ email: result.user.email });
      });
      await strapi.api.order.services.order.updateTotal(result._id)
      await strapi.api.order.services.order.sendOrderNotification(result._id)
      await strapi.api.order.services.order.sendOrderConfirmation(result._id)
    },
    afterUpdate: async (result) => {
      await strapi.api.order.services.order.updateTotal(result._id)
    }
  }
};
