'use strict';
const fs = require('fs')
const path = require('path')

const confirmationTemplate = fs.readFileSync(path.resolve(__dirname, '../../../public/confirmationTemplate.html'), 'utf8')
const notificationTemplate = fs.readFileSync(path.resolve(__dirname, '../../../public/notificationTemplate.html'), 'utf8')

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async updateTotal(id) {
    const orderModel = await strapi.query('order').model
    const order = await orderModel.findOne({ _id: id })
    const variantIds = order.Item.map(item => {
      return item.ref.product._id
    })
    const productVariants = await strapi.query('variant').find({ _id_in: variantIds })
    const variantLookup = productVariants.reduce((acc, variant) => {
      return {
        ...acc,
        [variant._id]: variant
      }
    }, {})
    const total = order.Item.reduce((acc, item) => {
      const currentVariant = variantLookup[item.ref.product._id]
      return acc + item.ref.amount * currentVariant.product.price
    }, 0)
    if (total !== order.total) {
      order.total = total
      await order.save()
    }
  },
  async sendOrderNotification(id) {
    if (process.env.EMAIL_ENABLED !== 'true') {
      strapi.log.warn('[E-MAIL SENDING DISABLED] Order Notification')
      return
    }
    const order = await strapi.query('order').findOne({ _id: id })
    const email = await strapi.services.config.getOrderNotificationEmail()
    await strapi.plugins.email.services.email.sendTemplatedEmail({
      to: email,
    }, {
      subject: 'Bestelleingang HandballIsmaning Shop',
      html: notificationTemplate,
      text: `
        Eine neue Bestellung ist eingegangen!

        Bestellnummer: #${order.number}
        Vorname: #${order.user.firstName}
        Nachname: #${order.user.lastName}
      `
    }, {
      number: order.number,
      total: order.total,
      items: order.Item.map((item) => ({
        name: item.product.fullName,
        amount: item.amount
      }))
    })
  },
  async sendOrderConfirmation(id) {
    if (process.env.EMAIL_ENABLED !== 'true') {
      strapi.log.warn('[E-MAIL SENDING DISABLED] Order Confirmation')
      return
    }
    const order = await strapi.query('order').findOne({ _id: id })
    await strapi.plugins.email.services.email.sendTemplatedEmail({
      to: order.user.email,
    }, {
      subject: 'Bestellbestätigung HandballIsmaning Shop',
      html: confirmationTemplate,
      text: `
        Wir haben Ihre Bestellung erhalten!

        Bestellnummer: #${order.number}

        Wir melden uns bei Ihnen bezüglich des weiteren Vorgehens
      `
    }, {
      number: order.number,
      total: order.total,
      items: order.Item.map((item) => ({
        name: item.product.fullName,
        amount: item.amount
      }))
    })
  }
};
