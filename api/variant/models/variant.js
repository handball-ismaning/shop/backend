'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */
async function getProduct(data) {
  const product = await strapi.query('product').findOne({ _id: data.product })
  if (!product) {
    throw new Error("Could not find associated product")
  }
  return product
}
function getFullName(data, product) {
  return product.name + ' - ' + data.name
}
async function updateProduct(result) {
  if (!result.product) {
    throw new Error('Related Product not found')
  }
  await strapi.api.product.services.product.updateStockCount(result.product._id)
}

module.exports = {
  lifecycles: {
    beforeCreate: async data => {
      const product = await getProduct(data)
      data.fullName = getFullName(data, product)
    },
    beforeUpdate: async (params, data) => {
      const product = await getProduct(data)
      data.fullName = getFullName(data, product)
    },
    afterCreate: async (result, data) => {
      await updateProduct(result)
    },
    afterUpdate: async (result, data) => {
      await updateProduct(result)
    }
  }
};
